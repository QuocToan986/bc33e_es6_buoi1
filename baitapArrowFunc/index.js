const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

let contentHTML = "";

let renderButtonColor = (list) => {
  let contentHTML = " ";
  list.forEach((color, index) => {
    contentHTML += `<button class = "color-button ${color}" onclick = "changColorHouse('${color}', ${index})" ></button>`;
  });
  document.getElementById("colorContainer").innerHTML = contentHTML;
};
renderButtonColor(colorList);

let changColorHouse = (color, index) => {
  let buttonNode = document.querySelectorAll("#colorContainer button");
  buttonNode.forEach((button) => {
    if (button !== buttonNode[index]) {
      button.classList.remove("active");
    }
  });
  buttonNode[index].classList.add("active");

  document.getElementById("house").className = `${color}`;
};
