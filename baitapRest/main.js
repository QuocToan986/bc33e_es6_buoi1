function tinhDTB(...rest) {
  let dtb = 0;
  let index = 0;
  for (; index < rest.length; index++) {
    dtb += rest[index];
  }
  return dtb / index;
}

document.getElementById("btnKhoi1").onclick = function () {
  let diemToan = document.getElementById("inpToan").value * 1;
  let diemLy = document.getElementById("inpLy").value * 1;
  let diemHoa = document.getElementById("inpHoa").value * 1;
  let dtbKhoi1 = tinhDTB(diemToan, diemLy, diemHoa);
  document.getElementById("tbKhoi1").innerText = dtbKhoi1.toFixed(1);
};

document.getElementById("btnKhoi2").onclick = function () {
  let diemVan = document.getElementById("inpVan").value * 1;
  let diemSu = document.getElementById("inpSu").value * 1;
  let diemDia = document.getElementById("inpDia").value * 1;
  let diemEnglish = document.getElementById("inpEnglish").value * 1;
  let dtbKhoi2 = tinhDTB(diemVan, diemSu, diemDia, diemEnglish);
  document.getElementById("tbKhoi2").innerText = dtbKhoi2.toFixed(1);
};
